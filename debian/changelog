fcitx-fbterm (0.2.0-4) unstable; urgency=medium

  * Team upload.
  * Rebuild before Ubuntu 22.04 LTS.
  * Refresh packaging:
    + Standards-Version: 4.6.0.
    + debhelper compat v13.
    + Rules-Requires-Root: no.
  * debian/control: Fix homepage field.
  * debian/copyright: Fix copyright information.

 -- Boyuan Yang <byang@debian.org>  Sat, 26 Feb 2022 10:09:00 -0500

fcitx-fbterm (0.2.0-3) unstable; urgency=medium

  * Team upload.
  * debian/control:
    - std-ver: 3.9.4 -> 4.1.5.
    - Bump debhelper compat to v11.
    - Use canonical Vcs URL on Salsa platform.
    - Use GitLab as homepage.
    - Use debian-input-method maillist in maintainer field.
      Closes: #899772.
    - Use @debian.org mail address for YunQiang Su in uploaders field.
  * Point d/watch at download.fcitx-im.org.
  * Apply "wrap-and-sort -abst".
  * debian/README.Debian: Rewrite document and usage.
  * d/rules:
    - Enable full hardening.
    - Use "dh_missing --fail-missing".
  * d/patches: Add a patch to point users to README.Debian file in
    "fcitx-fbterm-helper -h" output.

 -- Boyuan Yang <073plan@gmail.com>  Mon, 09 Jul 2018 21:47:55 +0800

fcitx-fbterm (0.2.0-2) unstable; urgency=low

  * Upload to unstable (Closes: #707377).
  * debian/control:
    - std-ver: 3.9.3 -> 3.9.4, no change.
    - remove obsolete DMUA field.
    - switch to canonical Vcs-* URLs.

 -- Aron Xu <aron@debian.org>  Sun, 12 May 2013 02:34:57 +0800

fcitx-fbterm (0.2.0-1) experimental; urgency=low

  * Imported Upstream version 0.2.0

 -- YunQiang Su <wzssyqa@gmail.com>  Wed, 26 Sep 2012 20:56:38 +0800

fcitx-fbterm (0.1.4-1) unstable; urgency=low

  * Fix debian/watch: googlecode
  * New Upstream version 0.1.4

 -- YunQiang Su <wzssyqa@gmail.com>  Tue, 15 May 2012 15:45:59 +0800

fcitx-fbterm (0.1.3-1) unstable; urgency=low

  * New upstream release.
  * Bump standard version to 3.9.3.

 -- YunQiang Su <wzssyqa@gmail.com>  Tue, 10 Apr 2012 15:44:11 +0800

fcitx-fbterm (0.1.2-2) unstable; urgency=low

  * Correct Build-Depends on fcitx-libs-dev (>= 1:4.2.0)
  * Set DMUA.

 -- Aron Xu <aron@debian.org>  Wed, 08 Feb 2012 02:20:03 +0800

fcitx-fbterm (0.1.2-1) unstable; urgency=low

  * New upstream release: for fcitx 4.2.0.
  * Drop 0001-use-bash-in-header.patch: applied upstream.

 -- YunQiang Su <wzssyqa@gmail.com>  Sun, 05 Feb 2012 22:49:47 +0800

fcitx-fbterm (0.1.1-2) unstable; urgency=low

  * Adjust package relations to be more user-friendly.

 -- Aron Xu <aron@debian.org>  Sat, 28 Jan 2012 23:53:08 +0800

fcitx-fbterm (0.1.1-1) unstable; urgency=low

  * Initial release (Closes: #639294)

 -- Aron Xu <aron@debian.org>  Wed, 26 Oct 2011 04:18:55 +0800
